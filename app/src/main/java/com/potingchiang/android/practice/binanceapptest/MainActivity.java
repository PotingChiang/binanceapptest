package com.potingchiang.android.practice.binanceapptest;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.potingchiang.android.practice.binanceapptest.pageradapter.MarketPagerAdapter;

public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText(R.string.title_home);
                    return true;
                case R.id.navigation_dashboard:
                    mTextMessage.setText(R.string.title_dashboard);
                    return true;
                case R.id.navigation_notifications:
                    mTextMessage.setText(R.string.title_notifications);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init view pager
        ViewPager marketViewPager = findViewById(R.id.market_view_pager);
        // init/setup pager adapter
        MarketPagerAdapter marketPagerAdapter
                = new MarketPagerAdapter(getSupportFragmentManager(), 5);
        marketViewPager.setAdapter(marketPagerAdapter);
        // init tab layout
        TabLayout marketTabLayout = findViewById(R.id.market_tab_layout);
        // setup view pager
        marketTabLayout.setupWithViewPager(marketViewPager);
        marketTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        mTextMessage = findViewById(R.id.message);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

}
