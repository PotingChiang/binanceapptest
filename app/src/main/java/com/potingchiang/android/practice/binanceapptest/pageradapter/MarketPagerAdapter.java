package com.potingchiang.android.practice.binanceapptest.pageradapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.potingchiang.android.practice.binanceapptest.markets.Dummy;

/**
 * pager adapter for Binance app test
 * Created by potingchiang on 2018-02-16.
 */

public class MarketPagerAdapter extends FragmentPagerAdapter {

    private final int tabCount;

    public MarketPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {

            case 0: {

            }
        }

        return null;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {


        switch (position) {

            case 0: {

                return "Favourite";
            }
            case 1: {

                return "BNB";
            }
            case 2: {

                return "BTC";
            }
            case 3: {

                return "ETH";
            }
            case 4: {

                return "USDT";
            }
        }

        return "Note set";
    }
}
